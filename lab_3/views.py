from django.shortcuts import render
from datetime import datetime, date
from lab_1.models import Friend
from lab_3.forms import FriendFrom
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required


@login_required(login_url='/admin/login/')
def index(request): 
    Friends = Friend.objects.all().values()  
    response = {'friends': Friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')    
def add_friend(request):
    context={}
    form = FriendFrom(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-3')

    context['form']= form
    return render(request, "lab3_form.html", context)


# Create your views here.
