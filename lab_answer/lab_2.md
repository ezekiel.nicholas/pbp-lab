1. What is the difference between JSON and XML?
There are several differences between JSON and XML. Here's the difference.
- XML is document oriented, JSON is data oriented
- XML requires tags to support arrays, while JSON already supports array access
- XML data is stored as a tree structure, while JSON is stored as a map containing keys and values
- The syntax in XML is more complicated and quite difficult to understand, while the syntax in JSON is simpler and easier to understand
- XML is a markup language, while JSON is a format written in JavaScript

2. What is the difference between HTML and XML?
Here's the difference between HTML and XML.
- XML is case sensitive, while HTML is case insensitive
- XML is supported by content, while HTML is supported by formats
- XML is centered on data transfer, while HTML is centered on data presentation
- XML stands for eXtensible Markup Language, while HTML stands for Hypertext Markup Language
- XML tags are not predefined, whereas HTML already has predefined tags