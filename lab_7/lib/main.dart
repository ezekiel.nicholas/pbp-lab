import 'package:flutter/material.dart';
import 'package:lab_7/screens/submit_form.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'Georgia',
        primaryColor: const Color.fromRGBO(0,173,181,1),
        secondaryHeaderColor: const Color.fromRGBO(79, 232, 180, 1),
        ),
      title: 'HeyDoc App',
      home: submit_form(),
      
    );
  }
}
