// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:lab_7/widgets/main_drawer.dart';

class submit_form extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
}

class FormScreenState extends State<submit_form> {
  final _formKey = GlobalKey<FormState>();
  var currentFocus;
  void _processData() {
    // Process your data and upload to server
    _formKey.currentState?.reset();
  }
  unfocus() {
    currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
            title: Text('HeyDoc'),
            backgroundColor: Theme.of(context).primaryColor),
        drawer: MainDrawer(),
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(30),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(16),
                  child: Text("Submit Form", style: TextStyle(fontSize: 30)),
                ),
                SizedBox(
                  height: 10,
                ),
                
                TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Title",
                    icon: Icon(Icons.title),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Title cannot be empty';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Article",
                    
                    icon: Icon(Icons.article),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  maxLines: 6,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Article cannot be empty';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Source",
                    icon: Icon(Icons.source),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Source cannot be empty';
                    }
                    return null;
                  },
                ),

                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        child: Text("Submit"),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            _processData();
                            showDialog(
                              context: context,
                              builder: (_) => AlertDialog(
                                title: Text("Update"),
                                content:
                                    Text("Your Article has been published"),
                                actions: [
                                  ElevatedButton(
                                    onPressed: () => Navigator.pop(context), 
                                    child: Text('Continue'),
                                  ),
                                ],
                              ),
                            );
                          }
                        },
                        color: Colors.green,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)),
                      ),
                      SizedBox(width: 30),
                      RaisedButton(
                        child: Text("Clear"),
                        onPressed: () => _processData(),
                        color: Colors.blueGrey[100],
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
