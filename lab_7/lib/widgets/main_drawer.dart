import 'package:flutter/material.dart';


class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, void Function() tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 20,
      ),
      title: Text(
        title,
        style: const TextStyle(
          fontSize: 20,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
          // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).secondaryHeaderColor,
            child: const Text(
              'HeyDoc',
              style: TextStyle(
                  fontSize: 30,
                  color: Color.fromRGBO(255, 255, 255, 1),)
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.home, () {
            Navigator.pop(context);
          }),
          buildListTile('Covid-19 Related Article', Icons.event, () {
            Navigator.pop(context);
          }),
          buildListTile('Vaccination Info', Icons.forum, () {
            Navigator.pop(context);
          }),
          buildListTile('Medicine', Icons.medication, () {
            Navigator.pop(context);
          }),
          buildListTile('Consultation', Icons.article, () {
            Navigator.pop(context);
          }),
          buildListTile('Admin', Icons.login, () {
            Navigator.pop(context);
          }),
          buildListTile('Logout', Icons.login, () {
            Navigator.pop(context);
          }),
          
        ],
      ),
    );
  }
}
