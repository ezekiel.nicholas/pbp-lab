import 'package:flutter/material.dart';
import 'package:lab_6/widgets/main_drawer.dart';
class submit_form extends StatelessWidget {
  const submit_form({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('HeyDoc'),
          backgroundColor: Theme.of(context).primaryColor
        ),
        drawer: MainDrawer(),
        body: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 20, bottom: 0),
                child: Text("Submit Form",
                style: TextStyle(fontSize: 30)),
                
              ),
              Container(
                
                padding: EdgeInsets.only(top: 42, left: 10, right: 10, bottom: 10),
                child: TextField(
                  autofocus: true,
                  style: TextStyle(fontSize: 15.0, color: Colors.black),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Title',
                    filled: true,
                    fillColor: Colors.grey,
                    contentPadding: const EdgeInsets.only(
                        left: 14.0, bottom: 6.0, top: 8.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Stack(
                  alignment: const Alignment(0, 0),
                  children: <Widget>[
                     TextFormField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Type Article Here',
                    filled: true,
                    fillColor: Colors.grey,
                    contentPadding: const EdgeInsets.only(
                        left: 14.0, bottom: 6.0, top: 8.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  style: TextStyle(color: Colors.black),
                  maxLines: 6,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Cannot be empty';
                    }
                    return null;
                  },
                ),

                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Stack(
                  alignment: const Alignment(0, 0),
                  children: <Widget>[
                     TextFormField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Source',
                    filled: true,
                    fillColor: Colors.grey,
                    contentPadding: const EdgeInsets.only(
                        left: 14.0, bottom: 6.0, top: 8.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  style: TextStyle(color: Colors.black),
                  maxLines: 1,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Cannot be Empty';
                    }
                    return null;
                  },
                ),

                  ],
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(10),
                  
                  child:  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        child: Text("Submit"),
                        onPressed: (){},
                        color: Colors.green,
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.green)),
                      ),
                      SizedBox(width: 30),
                      RaisedButton(
                        child: Text("Clear"),
                        onPressed: (){},
                        color: Colors.blueGrey[100],
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          ),
                      ),
                    ],
                    
                  ),
                )],
          ),
        ));
  }
}
